#!/bin/bash
## clones your dotfiles repo
git clone --bare https://gitlab.com/Delta1024/dotfiles.git $HOME/.cfg
git --git-dir="$HOME/.cfg" --work-tree="$HOME" checkout -f
## installs doom emacs to your user account
git clone --depth 1 https://github.com/hlissner/doom-emacs $HOME/.emacs.d
$HOME/.emacs.d/bin/doom install
## installes my build of dmenu to go with my dotfiles
git clone https://gitlab.com/Delta1024/pkgbuild.git $HOME/.pkgbuild
cd $HOME/.pkgbuild/dmenu
makepkg
## sets the background wallpaper
echo "[xin_-1]
file=/usr/share/backgrounds/archlinux/archwave.png
mode=0
bgcolor=#000000" >> /home/${user}/.config/nitrogen/bg-saved.cfg
echo "[geometry]
posx=10
posy=30
sizex=1004
sizey=728

[nitrogen]
view=icon
recurse=true
sort=alpha
icon_caps=false
dirs=/usr/share/backgrounds/archlinux;" >> /home/${user}/.config/nitrogen/nitrogen.cfg

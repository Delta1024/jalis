#!/bin/bash

read -p 'Grub Drive: ' sda # Drive to be formated
read -p 'hostname: ' hostname
read -p 'User Name: ' user
Region=Canada # ls /usr/share/zoneinfo
City=Mountain # ls /user/share/Region/
locgen='en_CA.UTF-8 UTF-8' #goes in /etc/local.gen
lang=en_CA.UTF-8 # language variable

ln -sf /usr/share/zoneinfo/${Region}/${City} /etc/localtime
hwclock --systohc

echo "###
#
# Locales Enabled by JALIS
${locgen}" >> /etc/locale.gen
locale-gen
echo "LANG=${lang}" >> /etc/locale.conf

echo "${hostname}" >> /etc/hostname
 echo "127.0.0.1     localhost
::1           localhost
127.0.1.1     ${hostname}.localdomain ${hostname}" > /etc/hosts

pacman -Syu --noconfirm
pacman -S --noconfirm grub
grub-install "/dev/${sda}"
grub-mkconfig -o /boot/grub/grub.cfg

useradd -mG wheel,audio,video "${user}"
echo '%wheel ALL=(ALL) ALL' >> /etc/sudoers
cd /root
mv /root/user-config.sh "/home/${user}/"
mv /root/yay.sh "/home/${user}/"
mv /root/yaylist.txt "/home/${user}/"

pacman -S --noconfirm --needed - < paclist.txt
systemctl disable iptables
systemctl enable ufw
systemctl enable lightdm
systemctl enable NetworkManager
git clone --bare https://gitlab.com/Delta1024/root-config.git /root/sys-config
git --git-dir=/root/sys-config --work-tree=/ $@ checkout -f
pacman -Syy

## executes user configuratoin script
pacman -S --noconfirm --needed go
su -c "/home/${user}/yay.sh" - ${user}
pacman -U --noconfirm /home/${user}/yay/*.pkg.*
rm -rf /home/${user}/yay/
su -c "/home/${user}/user-config.sh" - ${user}
pacman -R --noconfirm dmenu
pacman -U --noconfirm /home/${user}/.pkgbuild/dmenu/*.pkg.*

## remove configuration files and set passwords
rm /home/${user}/yay.sh
rm /home/${user}/user-config.sh
rm /root/paclist.txt
echo "Set Root Password"
passwd && echo "Set Password for ${user}" && passwd ${user}
rm /root/sys-config.sh

#!/bin/bash
#
## to generate paclist and yaylist run the following command:
## pacman -Qqe > yaylist.txt && comm -12 <(pacman -Slq | sort) <(sort yaylist.txt) > paclist.txt
#

## prepares the drives with the partition table in disks.sfdisk
## i forget how to make the table right now i'll look it up and add it latter

timedatectl set-ntp true
fdisk -l
read -p 'drive: ' drive
sfdisk /dev/${drive} < disks.sfdisk
mkfs.ext4 "/dev/${drive}2" && mkswap "/dev/${drive}1"
mount "/dev/${drive}2" /mnt
swapon "/dev/${drive}1"
pacstrap /mnt base linux linux-firmware vim git base-devel networkmanager
genfstab -U /mnt >> /mnt/etc/fstab
mv paclist.txt /mnt/root
mv yaylist.txt /mnt/root
mv yay.sh /mnt/root
mv sys-config.sh /mnt/root
mv user-config.sh /mnt/root
arch-chroot /mnt /root/sys-config.sh
reboot
